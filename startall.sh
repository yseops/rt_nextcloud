docker-compose --force-recreate --abort-on-container-exit --env-file ./traefik/.env  -f traefik/docker-compose.yml up -d
echo "started traefik"
docker-compose --force-recreate --abort-on-container-exit --env-file ./nginx/.env -f nginx/docker-compose.yml up -d
echo "started website"
docker-compose --force-recreate --abort-on-container-exit --env-file ./nextcloud/.env -f nextcloud/docker-compose.yml up -d
echo "started nextcloud"
docker-compose --force-recreate --abort-on-container-exit --env-file ./dozzle/.env  -f dozzle/docker-compose.yml up -d
echo "started dozzle"
