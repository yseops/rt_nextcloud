# PROJET NOTE:    
Membres du groupe :  Aymane AITAHMED, Lucas BOLORE, Sami ESSALHI, Sofiane CHIKAR.
---------------------------------------------------------------------------------------------------------------------

# Docker  test environment :
- Docker version 20.10.6, build 370c289
- docker-compose version 1.25.5, build 8a1c60f6.
Notre environment est en local sous Manjaro.
Nous avons utilisé Docker, Gitlab, Mkcert, Cypress, K6, Traefik, dozzele et Nginx pour réaliser le projet.


# L'objectif du projet :
## Etape 1: 
Un projet Open Source de votre choix qui remplit 
les conditions suivantes : 
- ● Une interface graphique où on peut se logguer
- ● Une base de données

Nous avons choisis de mettre en place **NEXTCLOUD** sous Docker.

     Nextcloud est une solution Cloud open source issue du fractionnement du projet ownCloud. Ses fonctionnalités     
     sont comparables à celles de solutions Cloud propriétaires comme Microsoft 365 ou Google Workspace. Nextcloud  
     synchronise les données telles que les calendriers et les contacts entre les différents utilisateurs et    
     appareils. 

# Mise en place : 
nous allons utiliser l’outil Docker Compose. 

Voici quelques-uns des différents services qu’il propose :
- [X]  _Proxy inverse Nginx_ : essentiel pour la création de connexions HTTPS cryptées lors de l’accès à Nextcloud


- [X]  _Mkcert_ (ne peut être utilisé que dans un environnement local) : est un outil permettant de générer des certificats de développement approuvés localement qui peuvent être partagés avec Traefik afin qu’il puisse effectuer un cryptage TLS.

- [X]  _MariaDB_ : permet de stocker les données générées (côté serveur) par l’utilisation de Nextcloud


- [X]  _Serveur Nextcloud_ : fournit les véritables fonctionnalités de Nextcloud en communiquant avec les clients et en hébergeant l’interface Web.

- [X] _Traefik_ : Traefik est un routeur Edge open-source qui fait de la publication de vos services une expérience amusante et facile. Il reçoit les demandes au nom de votre système et découvre quels composants sont responsables de leur traitement.

- [X] _Dozzle_ est une application simple et réactive qui vous fournit une interface Web pour surveiller vos journaux de conteneur Docker en direct. Il ne stocke pas les informations de journal, il est uniquement destiné à la surveillance en direct de vos journaux de conteneur.

# Installation l'environment Docker via docker compose :
     

1. Création d’un dossier de projet : 
    Le dossier nextcloud-docker dans le répertoire personnel de notre interface utilisateur.

2. Création des fichiers. Créez les fichiers  ** docker-compose.yaml**  et .env :

[docker-compose.yaml](https://gitlab.com/yseops/rt_nextcloud/-/blob/main/nextcloud/docker-compose.yml)


3. Création d’un réseau Docker. Utilisez la commande docker pour créer un nouveau réseau

Une fois ces structures créées, on poursuit l’installation de notre serveur Nextcloud.

On va commencer par créer un network qui sera partagé entre tous les conteneurs de nos applications. Nommé "proxy"

 ```docker network create proxy```



## Etape 2 : 

Monter une instance sur une VM locale, entrer quelques données :

-  Derrière un Traefik.
-  Le tout conteneurisé via docker-compose.yml
-  Simuler une activité l'ajout de quelques photos dans le cloud.
-  Derrière un nom de domaine (nextcloud.localhost) avec un certificat TSL géré par Traefik


**Gérer HTTPS avec docker-compose et mkcert pour le développement local :** 


Nous utiliserons l’image Docker pour démontrer l’utilisation d’un certificat TLS sur un service docker-compose.

Tout d’abord, nous devons créer et stocker les certificats TLS dans le dossier.
Ces certificats sont approuvés localement.

- Certificats mkcert :

```
`mkdir -p certs
mkcert -cert-file certs/local-cert.pem -key-file certs/local-key.pem `
```


- Configuration de Traefik :

- Créez ensuite les dossiers : ./docker/traefik

`mkdir -p docker/traefik`


- Créer le qui permet de configurer les sous-domaines disponibles desservis par Traefik.


Il déclare également le chemin des certificats TLS (générés avec mkcert) qui seront partagés avec Traefik via un volume Docker../docker/traefik/ [dynamic_conf.yaml](https://gitlab.com/yseops/rt_nextcloud/-/blob/main/traefik/volume/dynamic_conf.yml)

```
http:
  routers:
    traefik:
      rule: "Host(`traefik.app.localhost`)"
      service: "api@internal"
      tls:
        domains:
          - main: "app.localhost"
            sans:
              - "*.app.localhost"
          - main: "domain.local"
            sans:
              - "*.domain.local"

tls:
  certificates:
    - certFile: "/etc/certs/local-cert.pem"
      keyFile: "/etc/certs/local-key.pem"

```
Ensuite, on a créer le fichier de configuration `../docker/traefik/[traefik.yaml](https://gitlab.com/yseops/rt_nextcloud/-/blob/main/traefik/volume/traefik.yml)`

```
global:
  sendAnonymousUsage: false

api:
  dashboard: true
  insecure: true

providers:
  docker:
    endpoint: unix:///var/run/docker.sock
    watch: true
    exposedbydefault: false

  file:
    filename: /etc/traefik/dynamic_conf.yaml
    watch: true

log:
  level: DEBUG
  format: common

entryPoints:
  web:
    address: ':80'
    http:
      redirections:
        entryPoint:
          to: websecure
          scheme: https

  websecure:
    address: ':443'


```
**L'ajout des apps Docker dans Traefik :**

Dans le fichier [docker-compose.yml](https://gitlab.com/yseops/rt_nextcloud/-/blob/main/traefik/docker-compose.yml) 
Traefik utilise avec les labels de Docker que l'on positionne dans le fichier docker-compose.yml de notre app pour lui indiquer comment gérer le trafic de cette instance.


Forcer l'authentification dans le docker-compose.yml avec auth : 
On a également ajouté une authentification de base aux étiquettes avec les deux lignes suivantes. Identique au conteneur traefik, mais en remplaçant traefik par dozzle et en changeant la chaîne d'authentification USER:PASS.
```
- "traefik.http.middlewares.dozzle-auth.basicauth.users=USER:PASS"
- "traefik.http.routers.dozzle-secure.middlewares=secHeaders@file,dozzle-auth"
```


Dans le fichier .env, on a changé l'url.


Avec cette configuration, vous avez protégé l'accès au dashboard avec l'équivalent d'un fichier htpasswd. Veillez à bien doubler les $ dans la chaîne de caractère du mot de passe !



****- Configuration Navigateur :****

**Installation du certificat :**

Après avoir générer votre certificat au format pem , ouvrez Firefox et :

- Cliquez sur l’icône représentant trois petits barres horizontales en haut a droite de Firefox
- Cliquez sur "Options (Windows) / Préférences (Mac)"
- Sur la gauche, cliquez sur "Vie privée et sécurité"
- Descendez tout en bas puis cliquez sur "Afficher les certificats"
- Cliquez sur "Vos certificats"
- Cliquez sur "Importer"
- Dans la nouvelle fenêtre Sélectionnez le fichier .crt à installer
- Cliquez sur "Ouvrir"
- Saisissez le mot de passe de protection du certificat (qui a été défini lors de la génération du fichier pfx) et cliquez sur "OK"
- Un message apparaît vous confirmant la réussite de l'installation. Cliquez sur "OK"


Vérifier l'installation de votre certificat
Pour vérifier que l'installation s'est bien passée, suivez la procédure :

- Cliquez sur l’icône représentant trois petits barres horizontales en haut a droite de Firefox
- Cliquez sur "Options (Windows) / Préférences(Mac)"
- Sur la gauche, cliquez sur "Vie privée et sécurité"
- Descendez tout en bas puis cliquez sur "Afficher les certificats"
- Puis cliquez sur "Vos certificats" et faites un clic-gauche sur votre certificat
- Cliquez sur "Voir"
- Une fenêtre s'ouvre, allez dans l'onglet "Détails"
- Dans "Hiérarchie des certificats" vous devriez voir 3 éléments : Le certificat racine, l'intermédiaire et votre     certificat client.

Ici, nous avons 2 fournisseurs différents :


**DOCKER** : Utilisez des étiquettes de conteneurs pour récupérer la configuration du routage (plus d’informations dans la section suivante).


**FILE** : Charger un fichier de configuration. Voici ce que nous avons créé plus tôt.dynamic_conf.yaml



Exécuter la pile : 


Le plus dur est fait ! Il ne nous reste plus qu'à builder et démarrer notre conteneur :

Dans le fichier startall.sh on retrouve toutes les commandes qui vont builder l'ensemble.

`bash` [startall.sh](https://gitlab.com/yseops/rt_nextcloud/-/blob/main/startall.sh)

Et voilà! Vous devriez maintenant pouvoir aller à https://nextcloud.localhost/



**Nextcloud**
Après l'appel de l'url nextcloud, nextcloud doit être configuré comme ceci
```
<p align="center">
    <img src="/nextcloud.png" width="50%">
    <br/><br/>
</p>
```

Si vous obtenez une erreur d'autorisation refusée après avoir cliqué sur "terminer l'installation", vous devez supprimer les volumes et redémarrer les conteneurs.

### Ajout de conteneurs

Vous pouvez utiliser le fichier nginx docker-compose.yaml comme modèle.
Vous devez ajuster les étiquettes du nouveau conteneur dans le fichier docker-compose.yml. (voir exemple dans dozzle/docker-compose.yml)

À des fins de démonstration, On a ajouté dozzle. Avec dozzle, vous pouvez afficher les journaux de votre conteneur sur une page Web.


# Etape 3 :  TEST


-  Mise en place des outils :

Pour cela nous avons utilisé cypress, k6 et PHP

## 1. Partie test d’intégration :


- [X] Cypress: L’utilité de cypress est de dérouler un scénario complet de tests à la manière d’une situation réelle de production.
À travers cette scénarisation, l’objectif est bien de valider le logiciel développé et son association avec des tierces applications via API par exemple.

**Les pré-requis :**

- avoir installé le gestionnaire de paquets npm
Il suffit d’ouvrir un émulateur de terminal à partir du menu principal.

Une fois que vous êtes connecté, rafraîchissez le cache de l’APT. Pour ce faire, exécutez la commande suivante :

`sudo apt update`
Ainsi, nous nous assurons que les dépôts sont synchronisés et que le système peut télécharger en toute sécurité ce qui y est stocké.

Après cela, vous pouvez installer** Node.js **en exécutant la commande suivante :

`sudo apt install nodejs`

Après avoir saisi votre mot de passe, le processus d’installation commence.

Vous pouvez également **installer NPM**, qui est le gestionnaire de paquets node.js :

`sudo apt install npm`

Finalement, vérifiez la version installée. Pour cela, lancez la commande suivante :

USERam@PC:~$ nodejs -v
v12.22.12


**Cypress via :npm**

`cd /your/project/path`

`npm install cypress --save-dev`

Cela installera Cypress localement en tant que dépendance de développement pour votre projet.

Ensuite nous avons lancé cypress et commencé les tests en ouvrant un navigateur avec la commande :

`npx cypress open`
 
Nous avons configuré cypress via le fichier [cypress.config.ts](https://gitlab.com/yseops/rt_nextcloud/-/blob/main/nextcloud-cypress/cypress.config.ts)

Nous avons choisi de mettre en place 3 tests : 

**- Visiter la page principale**
- Login / logout
- Le bouton upload


Pour cela, nous avons créé les fichers suivants : session.cy.ts & uploadPicker.cy.ts
exemple de test cypress : 
```
describe('Login and logout', function() {
it('Login and see the default files list', function() {
cy.visit('/apps/files')
cy.url().should('include', '/login')
cy.login('admin', 'admin')
cy.url().should('include', '/apps/files')
})

it('Logout and see the login page', function() {
cy.visit('/apps/files')
cy.url().should('include', '/apps/files')
cy.logout()
cy.url().should('include', '/login')
})
})
```

**TEST OK Login Logout
 **




## 2. Partie test de performance et de charge:


- [X] Les tests de performance sont le processus d’analyse de la qualité et de la capacité d’un produit. Les tests de charge sont un sous-ensemble des tests de performances où nous analysons le comportement de l’application dans des conditions de charge normales ou de pointe. e.

**Pré-requis :**

- Installer k6 :

`sudo apt-get install k6`
Un exemple simple qui exécute simplement une requête GET ressemble à ceci :

**Javascript: **

```
import http from 'k6/http';
import { sleep } from 'k6';export default function () {
  http.get('https://nextcloud.sdvnext.codelib.re/');
  sleep(1);
}
```

Exécuter le test localement à l’aide de la commande suivante.

`k6 run loadtest.js`

Cela produit la sortie suivante :

[CAPTURE K6 SORTIE ](https://i.imgur.com/GPgX37g.png)
 
# 3. La CI Gitlab :

Tous les tests pertinents avec les outils associés :


  - build
  - lint
  - test units
  - test integration
  - test end to end
  - deploy
 
# Procédure de migration :

### Préparer puis effectuer une migration :
Compte sur **Azure** :

On se connecte sur la VM pour la démarrer et récupérer l'IP.
-  Migrer les données :


CI fait maintenant partie de GitLab. Vous n’avez plus besoin d’exécuter une instance distincte du serveur CI. Ce guide vous guide tout au long de la procédure de migration de vos données CI existantes dans GitLab.8.0.0


**Étape 1 - Préparer la migration**

 Arrêtez vos serveurs GitLab et CI

**Étape 2 - Mise à niveau vers les versions 7.14.3**

 La migration vers GitLab ne peut être effectuée qu’à partir de la version . Par conséquent, nous devons d’abord migrer vers les versions les plus récentes de ces images.8.07.14.3

Mise à niveau vers gitlab:7.14.3
Mise à niveau vers  gitlab-ci:7.14.3-1


**Étape 3 - Générer des sauvegardes**

- Créez des sauvegardes pour nous assurer que nous pouvons revenir en arrière au cas où vous rencontreriez des problèmes pendant la migration
- Créer une sauvegarde GitLab

Notez l’archive de sauvegarde car c’est la sauvegarde vers laquelle vous devrez revenir en cas d’erreur.xxxxxxxxxx_gitlab_backup.tar

- Créer une sauvegarde GitLab CI

**Étape 4 - Mettre à niveau CI**

CI est uniquement destiné à la migration vers GitLab. Ici, nous devons passer à la version et générer une sauvegarde qui sera importée dans GitLab.8.x.x8.08.x.x

- Créer une sauvegarde CI

**Étape 5 - Mettre à niveau GitLab**

1. Mise à niveau vers /gitlab:8.0.5-1
1. Migrer les données CI

**Étape 6 - Correction des configurations DNS et proxy inverse**


Puisque GitLab et CI ne font plus qu’un, mettez à jour votre configuration **DNS** pour vous assurer qu’il pointe vers notre instance nextcloud.localhost.

On a utilisez un proxy nginx, pour mettre à jour la configuration de manière à ce qu’elle s’interface avec le serveur GitLab nextcloud.localhost

Si vous modifiez l’URL sur les coureurs, vous pouvez également supprimer complètement le nom de domaine.

**Étape 7 - Terminé !**


Vous pouvez maintenant démarrer le serveur GitLab normalement. 
Assurez-vous que et sont définis dans votre environnement de conteneurs.


Voici notre production disponible sur internet via AZURE : 

 [DASHBOARD AZURE](https://i.imgur.com/GAxG5jn.jpg)

1. [DASHBOARD consulter les log](https://i.imgur.com/9UJ1Glo.jpg)s : 

https://logs.sdvnext.codelib.re/


2. [DASHBOARD de Traefik](https://i.imgur.com/8H0FCPz.jpg) : 

https://traefik.sdvnext.codelib.re/dashboard/#/


3. [DASHBOARD Nextcloud ](https://i.imgur.com/cqVNZI5.jpg): 

https://nextcloud.sdvnext.codelib.re/apps/dashboard/






### Contribution
All contributions are welcome.
